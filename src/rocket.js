import * as movement from "./movement.js";
import * as modal from "./modal/modal.js";
let app = new PIXI.Application({
  antialias: false,
  transparent: true,
  resolution: 1,
  width: document.body.clientWidth,
  height: 550
});

PIXI.Loader.shared
  .add(["../assets/rocket.png"])
  .add(["../assets/thrust.png"])
  .load(Rocket);
let texture = PIXI.Texture.from("../assets/rocket_top.png");
const url = "https://api.spacexdata.com/v2/rockets";

document.body.appendChild(app.view);

let date = new Date();
let bodyArr = [];
let thrustArr = [];
let fuelArr = [];

function Rocket() {
  const https = new XMLHttpRequest();
  let responsePayload = [];
  https.open("GET", url, true);
  https.onload = async function () {
    responsePayload = getPayload(this);
    let rocketArr = initRockets(responsePayload);
    //***IF YOU WANT THINGS TO HAPPEN FASTER,JUST REMOVE THE COMMENTS BELOW  */

    // responsePayload[0].first_stage.fuel_amount_tons = 12;
    // responsePayload[1].first_stage.fuel_amount_tons = 10;
    // responsePayload[1].second_stage.fuel_amount_tons = 12;
    // responsePayload[2].first_stage.fuel_amount_tons = 12;
    // responsePayload[2].second_stage.fuel_amount_tons = 14;
    // responsePayload[3].first_stage.fuel_amount_tons = 5;
    // responsePayload[3].second_stage.fuel_amount_tons = 17;
    app.ticker.add(delta => gameLoop(delta, responsePayload, rocketArr));
  };

  https.send();
}

function initRockets(responsePayload) {
  let rocketArr = [];
  for (let i = 0; i < responsePayload.length; i++) {
    let body = new PIXI.Sprite(
      PIXI.Loader.shared.resources["../assets/rocket.png"].texture
    );
    let thrust = new PIXI.Sprite(
      PIXI.Loader.shared.resources["../assets/thrust.png"].texture
    );
    fuelArr.push(responsePayload[i].first_stage.fuel_amount_tons);
    bodyArr.push(body);
    thrustArr.push(thrust);
    thrust.x = 360 + i * 200;
    thrust.y = 595;
    body.x = 350 + i * 200;
    body.y = 500;
    app.stage.addChild(body);
    app.stage.addChild(thrust);
    rocketArr.push({
      body: body,
      thrust: thrust
    });
  }
  return rocketArr;
}

function gameLoop(delta, responsePayload, rocketArr) {
  let currentDate = new Date();
  while (currentDate.getTime() - date.getTime() > 1000) {
    date = new Date();
    for (let ind = 0; ind < responsePayload.length; ind++) {
      fuelArr[ind] = responsePayload[ind].first_stage.fuel_amount_tons;
      if (responsePayload[ind].first_stage.fuel_amount_tons <= 0) {
        bodyArr[ind].texture = texture;
        responsePayload[ind].second_stage.fuel_amount_tons--;
        if (responsePayload[ind].second_stage.fuel_amount_tons <= 0) {
          bodyArr[ind].visible = false;
          if (rocketArr[ind].thrust.parent !== null) {
            rocketArr[ind].thrust.parent.removeChild(thrustArr[ind]);
            modal.isEnd(rocketArr);
          }
        }
      } else if (responsePayload[ind].first_stage.fuel_amount_tons > 0) {
        responsePayload[ind].first_stage.fuel_amount_tons--;
      }
    }
  }
  movement.rocketMovementChange(rocketArr, delta);
}

function getPayload(response) {
  if (response.readyState == 4 && response.status === 200) {
    return JSON.parse(response.response);
  }
  return "";
}

export {
  Rocket,
  app
};
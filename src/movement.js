function rocketMovementChange(rocketArr, delta) {
     for (let i = 0; i < rocketArr.length; i++) {
          if (rocketArr[i].body.y > 0) {
               rocketArr[i].body.y -= 0 + delta;
               rocketArr[i].thrust.y -= 0 + delta;
          }
          if (rocketArr[i].thrust.visible) {
               setTimeout(function () {
                    rocketArr[i].thrust.visible = false;
               }, 50);
          } else {
               setTimeout(function () {
                    rocketArr[i].thrust.visible = true;
               }, 50);
          }
     }
}

export {
     rocketMovementChange
};
let modal = document.querySelector(".modal");

function toggleModal() {
     modal.classList.toggle("show-modal");
}

function isEnd(rocketArr) {
     if (rocketArr.every(element => element.thrust.parent === null)) {
          return document
               .querySelector(".trigger")
               .addEventListener("click", toggleModal());
     }
}

export {
     isEnd
}